# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_km.po to Khmer
# translation of km.po to
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt#
#
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2006, 2007, 2008, 2010.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po_sublevel1_km\n"
"Report-Msgid-Bugs-To: finish-install@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:04+0000\n"
"PO-Revision-Date: 2010-06-21 09:08+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer <support@khmeros.info>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../finish-install.templates:1001
msgid "Finish the installation"
msgstr "បញ្ចប់​ការ​ដំឡើង​"

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:2001
msgid "Finishing the installation"
msgstr "កំពុង​បញ្ចប់​ការ​ដំឡើង​"

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:3001
msgid "Running ${SCRIPT}..."
msgstr "កំពុង​រត់​ ${SCRIPT}..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:4001
msgid "Configuring network..."
msgstr "កំពុង​កំណត់​រចនា​សម្ព័ន្ធ​បណ្តាញ..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:5001
msgid "Setting up frame buffer..."
msgstr "កំពុង​រៀបចំ frame buffer..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:6001
msgid "Unmounting file systems..."
msgstr "កំពុង​អាន់ម៉ោន​ប្រព័ន្ធ​ឯកសារ..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:7001
msgid "Rebooting into your new system..."
msgstr "កំពុង​ចាប់ផ្ដើម​ប្រព័ន្ធ​ថ្មី​របស់​អ្នក..."

#. Type: note
#. Description
#. :sl1:
#: ../finish-install.templates:8001
msgid "Installation complete"
msgstr "ដំឡើង​រួចរាល់​ហើយ"

#. Type: note
#. Description
#. :sl1:
#: ../finish-install.templates:8001
#, fuzzy
#| msgid ""
#| "Installation is complete, so it is time to boot into your new system. "
#| "Make sure to remove the installation media (CD-ROM, floppies), so that "
#| "you boot into the new system rather than restarting the installation."
msgid ""
"Installation is complete, so it is time to boot into your new system. Make "
"sure to remove the installation media, so that you boot into the new system "
"rather than restarting the installation."
msgstr ""
"ដំឡើង​បាន​រួចរាល់​ហើយ ដូច្នេះ​ដល់​ពេល​ចាប់​ផ្តើម​ប្រព័ន្ធ​ថ្មី​របស់​អ្នក​ហើយ ។ សូម​ធ្វើ​ឲ្យ​ប្រាកដ​ថា​បាន​ដក​មេឌៀ​"
"ដំឡើង (ស៊ីឌីរ៉ូម, ថាសទន់) ដូច្នេះ​ទើប​អាច​ចាប់ផ្ដើម​ប្រព័ន្ធ​ថ្មី​បាន បើ​មិន​អញ្ចឹង​ទេ អ្នក​នឹង​ចាប់ផ្ដើម​ការ​"
"ដំឡើង​ម្ដង​ទៀត​ហើយ ។"
